package org.flip.initiate.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.flip.initiate.model.CustomerInfo;
import org.flip.initiate.model.CustomerRepository;
import org.flip.initiate.model.Products;
import org.flip.initiate.model.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class AdapterWrapperClass {
	
	@Autowired
	private CustomerRepository repository_info;
	
	@Autowired
	private ProductsRepository repository_products;
	
	private void addNewCustomer(CustomerInfo customerInfo) {
		repository_info.save(customerInfo);
	}
	
	private List<Products> getCart(CustomerInfo customerInfo) {
		
		List<Products> cartProducts= new ArrayList<>();
		
		int quantity=0;
		
		for(Products list : customerInfo.getProductList()) {
			if(repository_products.findByName(list.getName()) != null) {
				Products product= repository_products.findByName(list.getName());
				if(product.getQuantity()>0) {
					product.setOutOfStock(false);
					quantity=product.getQuantity()-1;
					product.setQuantity(quantity);
					repository_products.save(product);
					cartProducts.add(list);
				}
				else {
					product.setOutOfStock(true);
					cartProducts.add(product);
				}
			}
		}
		
		if(repository_info.findByFirstName(customerInfo.getFirstName())!=null) {
			customerInfo.setProductList(cartProducts);
			repository_info.save(customerInfo);
		}
		
		return cartProducts;
	}

}
