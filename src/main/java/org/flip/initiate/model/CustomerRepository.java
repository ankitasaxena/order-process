package org.flip.initiate.model;

import org.springframework.data.mongodb.repository.MongoRepository;;
public interface CustomerRepository extends MongoRepository<CustomerInfo, String>{
	
	public CustomerInfo findByFirstName(String firstName);

}
