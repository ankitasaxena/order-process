package org.flip.initiate.model;

import java.io.Serializable;
import java.util.List;

public class CartPayload implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Products> listOfProducts;
	private boolean outOfStockFlag;
	public List<Products> getListOfProducts() {
		return listOfProducts;
	}
	public void setListOfProducts(List<Products> listOfProducts) {
		this.listOfProducts = listOfProducts;
	}
	public boolean isOutOfStockFlag() {
		return outOfStockFlag;
	}
	public void setOutOfStockFlag(boolean outOfStockFlag) {
		this.outOfStockFlag = outOfStockFlag;
	}
	
	
	
}
