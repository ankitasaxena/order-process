package org.flip.initiate.model;

import org.springframework.data.mongodb.repository.MongoRepository;;
public interface ProductsRepository extends MongoRepository<Products, String>{
	
	public Products findByName(String name);

}
