package org.flip.initiate.model;

import java.util.List;

import org.springframework.data.annotation.Id;

public class CustomerInfo {
	
	@Id
	private String id;
	
	private String firstName;
	private String lastName;
	private String houseAddress;
	private List<Products> productList;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getHouseAddress() {
		return houseAddress;
	}
	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	 @Override
	    public String toString() {
	        return String.format(
	                "CustomerInfo[id=%s, firstName='%s', lastName='%s']",
	                id, firstName, lastName);
	    }
	public List<Products> getProductList() {
		return productList;
	}
	public void setProductList(List<Products> productList) {
		this.productList = productList;
	}
	 
}
